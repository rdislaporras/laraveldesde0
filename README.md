> # laravelDesde0
> 
> Proyecto hecho en laravel.
> Pensado para ser una wiki con ejemplos practicos de lo que voy aprendiendo de acuerdo con los requisitos del dia dia.


**¿Qué es Laravel?**

Laravel es uno de los frameworks de código abierto más fáciles de asimilar para PHP. Es simple, muy potente y tiene una interfaz elegante y divertida de usar. Fue creado en 2011 y tiene una gran influencia de frameworks como Ruby on Rails, Sinatra y ASP.NET MVC.
El objetivo de Laravel es el de ser un framework que permita el uso de una sintaxis refinada y expresiva para crear código de forma sencilla, evitando el “código espagueti” y permitiendo multitud de funcionalidades. Aprovecha todo lo bueno de otros frameworks y utiliza las características de las últimas versiones de PHP.
En esta wiki explicamos de manera gratuita los fundamentos del framework Laravel, incluyendo el ORM Eloquent, su sistema de migraciones, seeders y constructor de consultas, el sistema de plantillas Blade, rutas, controladores y mucho más.
Durante el curso crearemos un proyecto sencillo pero funcional con módulos de tipo CRUD. Además como un bonus opcional en este curso verás una introducción básica y gratuita a TDD con Laravel.
Para más iformación sobre TDD la tienes aqui.
Aquí puedes encontrar la Documentación Oficial de Laravel
Que Vamos a explorar en esta wiki?. Pues a continuación mostramos una serie de lista en la que nos guiaremos:
Conociendo Laravel


***Preparación del ambiente***

*  [Instalación de LocalHost y Composer](https://gitlab.com/rdislaporras/laraveldesde0/wikis/Instalaci%C3%B3n-de-LocalHost-y-Composer)
*  [Instalación de Laravel](https://gitlab.com/rdislaporras/laraveldesde0/wikis/Instalaci%C3%B3n-de-Laravel---Creaci%C3%B3n-de-proyecto)
*  [Creación del Proyecto](https://gitlab.com/rdislaporras/laraveldesde0/wikis/Creaci%C3%B3n-del-proyecto)

 
***Conociendo Laravel***
* [Estructura de laravel](https://gitlab.com/rdislaporras/laraveldesde0/wikis/Estructura-del-Proyecto-Laravel) 
* [Primeros Pasos en el proyecto](https://gitlab.com/rdislaporras/laraveldesde0/wikis/Primeros-pasos-en-el-proyecto)
*  
* Rutas
* Pruebas
* Controladores



**Vistas**

* Vistas
* Plantillas con Blade
* Layouts con Blade


**Manejo de Bases de datos con Laravel**

* Introducción al manejo de base de datos 
* Modificar tablas ya existentes usando migraciones
* Creación y asociación de tablas con el uso de migraciones y claves foráneas
* Inserción de datos con el uso de Seeders 
* Constructor de consultas SQL 
* Introducción a Eloquent ORM 
* Usando Eloquent ORM de forma interactiva con Tinker 
* Manejo de atributos en Eloquent ORM 
* Relaciones con Eloquent ORM 
* Model Factories 


**Ejercicios Practicos: En esta sección iremos implementando poco a poco cada uno de los conocimientos adquiridos.**

1. Crud Básico de usuarios y uso de plantillas con Bootsrap 